package config

type Emerge struct {
	CommonArgs []string `json:"common_args"`
}

type Config struct {
	SyncTries    int    `json:"sync_tries"`
	UpgradeTries int    `json:"upgrade_tries"`
	Emerge       Emerge `json:"emerge"`
}

var Cfg Config
