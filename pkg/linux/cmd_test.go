package linux

import (
	"bytes"
	"fmt"
	"os/exec"
	"testing"
)

func TestCommand_Run(t *testing.T) {
	c := new(Command)
	c.Cmd = exec.Command("./cmd_test.sh")
	c.Desc = "Testing ..."
	if err := c.Run(); err != nil {
		t.Fatal(err)
	}
	out := []byte{49, 106, 100, 107, 108, 115, 97, 106, 105, 106, 100, 106, 106, 100, 107, 108, 32, 100, 106, 100, 106, 107, 100, 10, 51, 10, 53, 32, 100, 106, 107, 108, 49, 105, 49, 106, 107, 108, 106, 100, 32, 100, 106, 108, 100, 10, 55, 10}
	err := []byte{50, 32, 106, 100, 32, 106, 107, 32, 106, 107, 100, 108, 115, 97, 10, 52, 10, 54, 10, 56, 32, 106, 106, 106, 106, 100, 108, 49, 106, 107, 40, 40, 40, 38, 57, 56, 101, 49, 41, 41, 41, 10}

	identical := true
	if bytes.Compare(out, c.StdOut) != 0 {
		fmt.Printf("Stdout expect: %v\n", out)
		fmt.Printf("Stdout actual: %v\n", c.StdOut)
		identical = false
	}
	if bytes.Compare(err, c.StdErr) != 0 {
		fmt.Printf("Stderr expect: %v\n", err)
		fmt.Printf("Stderr actual: %v\n", c.StdErr)
		identical = false
	}
	if !identical {
		t.Fatal()
	}
}
