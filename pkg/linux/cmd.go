package linux

import (
	"io"
	"log"
	"os"
	"os/exec"
)

type Command struct {
	Cmd    *exec.Cmd
	Desc   string
	StdOut []byte
	StdErr []byte
}

// NewCommand is used to generate a new Command object with a command name and optional arguments,
// it will also append common arguments which are set from configuration files (emerge.common_args).
func NewCommand(name string, arg ...string) *Command {
	cmd := new(Command)
	cmd.Cmd = exec.Command(name, arg...)
	return cmd
}

// Run prints the descriptions, prompts, and executes the associated Command,
// it prints the stdout/stderr and save to Command.StdOut and Command.StdErr respectively.
func (c *Command) Run() error {
	if c.Desc != "" {
		log.Println(c.Desc)
	}
	log.Printf("\x1b[1m\x1b[32m>>>\x1b[0m %s\n", c.Cmd.String())

	stdout, err := c.Cmd.StdoutPipe()
	if err != nil {
		return err
	}
	stderr, err := c.Cmd.StderrPipe()
	if err != nil {
		return err
	}

	err = c.Cmd.Start()
	if err != nil {
		return err
	}

	o := io.TeeReader(stdout, os.Stdout)
	e := io.TeeReader(stderr, os.Stderr)
	var oo, ee []byte
	ch0 := make(chan bool)
	ch1 := make(chan bool)
	go func() {
		oo, err = io.ReadAll(o)
		if err != nil {
			log.Println(err)
		}
		ch0 <- true
	}()
	go func() {
		ee, err = io.ReadAll(e)
		if err != nil {
			log.Println(err)
		}
		ch1 <- true
	}()
	<-ch0
	<-ch1

	err = c.Cmd.Wait()

	c.StdOut = make([]byte, len(oo))
	c.StdErr = make([]byte, len(ee))
	copy(c.StdOut, oo)
	copy(c.StdErr, ee)

	log.Printf("\x1b[1m<<<\x1b[0m %s\n", c.Cmd.String())
	return err
}
