package notification

func Send(msg string) error {
	return nil
}

func SendErr(e error) error {
	return Send(e.Error())
}
