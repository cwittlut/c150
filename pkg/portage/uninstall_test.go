package portage

import (
	"c150/pkg/config"
	"testing"
)

func TestUninstall(t *testing.T) {
	config.Cfg.Emerge.CommonArgs = []string{"-vp"}
	_, err := Uninstall([]string{"net-wireless/iw", "dev-lang/go"}, false)
	if err != nil {
		t.Fatal(err)
	}
}
