package portage

import (
	"c150/pkg/config"
	"testing"
)

func TestInstall(t *testing.T) {
	config.Cfg.Emerge.CommonArgs = []string{"-jvp", "--keep-going", "-O"}
	_, err := Install([]string{"www-servers/nginx", "dev-lang/go"}, true, "-*")
	if err != nil {
		t.Fatal(err)
	}
}
