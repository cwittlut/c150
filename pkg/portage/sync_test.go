package portage

import "testing"

func TestSync(t *testing.T) {
	if _, err := Sync(); err != nil {
		t.Fatal(err)
	}
}
