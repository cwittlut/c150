package portage

import (
	"c150/pkg/config"
	"c150/pkg/linux"
	"fmt"
	"regexp"
	"strings"
)

var selectPkgsRegex, _ = regexp.Compile(`\nAll\sselected\spackages:\s(.*)\n`)

type DepError struct {
	Atom     []string
	RealAtom []string
}

func (e DepError) Error() string {
	return fmt.Sprintf("cannot solve all atoms. provided atom: %s; solved atom: %s\n",
		strings.Join(e.Atom, ", "), strings.Join(e.RealAtom, ", "))
}

func Uninstall(atom []string, force bool) (cmd *linux.Command, err error) {
	var realAtom []string

	if !force {
		arg := append([]string{"-vp", "--depclean"}, atom...)
		cmdCheck := linux.NewCommand("emerge", arg...)
		cmdCheck.Desc = fmt.Sprintf("Checking dependencies before unmerging %s ...", strings.Join(atom, ", "))

		err = cmdCheck.Run()
		if err != nil {
			return cmdCheck, err
		}

		matches := selectPkgsRegex.FindSubmatch(cmdCheck.StdOut)
		if len(matches) >= 2 {
			realAtom = append(realAtom, strings.Split(string(matches[1]), " ")...)
		}

		if len(realAtom) != len(atom) {
			return nil, DepError{Atom: atom, RealAtom: realAtom}
		}
	}

	var a *[]string
	if len(realAtom) > 0 {
		a = &realAtom
	} else {
		a = &atom
	}

	arg := append([]string{"-C"}, config.Cfg.Emerge.CommonArgs...)
	arg = append(arg, *a...)
	cmd = linux.NewCommand("emerge", arg...)
	cmd.Desc = fmt.Sprintf("Uninstalling %s ...", strings.Join(*a, ", "))

	err = cmd.Run()
	return
}
