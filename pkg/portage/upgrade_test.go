package portage

import (
	"c150/pkg/config"
	"testing"
)

func TestUpgrade(t *testing.T) {
	config.Cfg.Emerge.CommonArgs = []string{"-jv", "--keep-going"}
	_, err := Upgrade()
	if err != nil {
		t.Fatal(err)
	}
}
