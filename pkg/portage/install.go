package portage

import (
	"c150/pkg/config"
	"c150/pkg/linux"
	"fmt"
	"os"
	"strings"
)

func Install(atom []string, oneShot bool, use string) (cmd *linux.Command, err error) {
	arg := append([]string{}, config.Cfg.Emerge.CommonArgs...)
	if oneShot {
		arg = append(arg, "-1")
	}
	arg = append(arg, atom...)

	err = os.Setenv("USE", use)
	if err != nil {
		return
	}

	cmd = linux.NewCommand("emerge", arg...)
	cmd.Desc = fmt.Sprintf("Installing %s ...", strings.Join(atom, ", "))

	err = cmd.Run()

	return
}
