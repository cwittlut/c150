package portage

import (
	"c150/pkg/config"
	"c150/pkg/linux"
)

func Upgrade() (cmd *linux.Command, err error) {
	arg := append([]string{}, config.Cfg.Emerge.CommonArgs...)
	arg = append(arg, "-uDN", "@world")
	cmd = linux.NewCommand("emerge", arg...)
	cmd.Desc = "Updating @world set ..."

	err = cmd.Run()

	return
}
