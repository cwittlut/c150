package portage

import (
	"c150/pkg/linux"
)

func Sync() (cmd *linux.Command, err error) {
	cmd = linux.NewCommand("emerge", "--sync")
	cmd.Desc = "Syncing portage database ..."

	err = cmd.Run()

	return
}
