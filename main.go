//go:build unix

package main

import (
	"c150/pkg/config"
	"c150/pkg/linux"
	"c150/pkg/notification"
	"c150/pkg/portage"
	"log"
	"os/user"
)

func upgrade() (err error) {
	var cmd *linux.Command
	for i := 0; i < config.Cfg.SyncTries; i++ {
		cmd, err = portage.Sync()
		if err == nil {
			break
		} else {
			log.Println("try syncing again ...")
		}
	}
	if err != nil {
		return err
	}

	for i := 0; i < config.Cfg.UpgradeTries; i++ {
		cmd, err = portage.Upgrade()
		if err == nil {
			break
		} else {
			log.Println("try upgrading again ...")
		}
	}
	if err != nil {
		return err
	}

	return err
}

func main() {
	u, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	if u.Uid != "0" {
		log.Fatal("Should be run with 'root' user.")
	}

	err = upgrade()
	if err != nil {
		if err = notification.SendErr(err); err != nil {
			log.Fatal(err)
		}
	}
}
